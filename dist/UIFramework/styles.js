(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./src/styles.css":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./src/styles.css ***!
  \*****************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "/* You can add global styles to this file, and also import other style files */\r\n\r\n/* @import '/app/components/products/arthrid/arthrid.component.css';\r\n@import '/app/components/products/arthrid/memoplusgold.component.css';\r\n@import '/app/components/products/arthrid/cogentdb.component.css'; */\r\n\r\nbody {\r\n\t/* max-width: 1800px; */\r\n\tmargin: 0 auto;\r\n\t/* font-family: 'Avante-Black' !important; */\r\n\tfont-family: 'Open Sans', sans-serif;\r\n\toverflow-x: hidden;\r\n}\r\n\r\n.link {\r\n\tcolor: white !important;\r\n\t/* For Safari 3.0 to 6.0 */\r\n\ttransition: font-size 2s;\r\n\t/* For modern browsers */\r\n}\r\n\r\n.link:hover {\r\n\ttext-shadow: 2px 2px gray;\r\n\tfont-size: 110%;\r\n\tfont-weight: bolder;\r\n}\r\n\r\n.divider {\r\n\tdisplay: inline-block;\r\n\twidth: 0;\r\n\theight: 3em;\r\n\tborder-right: white solid 3px;\r\n}\r\n\r\nh1,\r\nh2,\r\nh3,\r\nh4 {\r\n\tfont-family: 'CooperHewitt' !important;\r\n}\r\n\r\n@font-face {\r\n\tfont-family: \"CooperHewitt\";\r\n\tsrc: url(\"/assets/fonts/CooperHewitt-Bold.otf\");\r\n}\r\n\r\n@font-face {\r\n\tfont-family: \"Avante-Bold\";\r\n\tsrc: url(\"/assets/fonts/ITCAvantGardeStd-BoldCn.otf\");\r\n}\r\n\r\n.w-15 {\r\n\twidth: 15%;\r\n}\r\n\r\n.w-5 {\r\n\twidth: 5%;\r\n}\r\n\r\n.uk-card-title {\r\n\tfont-size: 1.2em !important;\r\n\tline-height: 1.4;\r\n}\r\n\r\n.round-border {\r\n\tborder-radius: 50px;\r\n}\r\n\r\n.green {\r\n\tbackground-color: #cfe5ae !important;\r\n}\r\n\r\n.teal {\r\n\tbackground-color: #fff9ae !important;\r\n}\r\n\r\n.powderblue {\r\n\t/* background-color: #48c4d3 !important; */\r\n\tbackground-image: url('/assets/products/arthrid/arthrid-gradient.png');\r\n\tbackground-position: center;\r\n\tbackground-size: cover;\r\n\tbackground-repeat: no-repeat;\r\n}\r\n\r\n#landingPage {\r\n\twidth: 100vw;\r\n\theight: 100vh;\r\n}\r\n\r\n#menu > li:nth-last-child(2) {\r\n\tborder-right: none;\r\n}\r\n\r\n.title-header {\r\n\tcolor: #be1e2d;\r\n}\r\n\r\n#ing-comp {\r\n\tbackground: url('/assets/products/cogent/ing-bg.jpg');\r\n\tbackground-attachment: fixed;\r\n\tbackground-size: cover;\r\n\tbackground-position-y: calc(0px);\r\n\tbackground-repeat: no-repeat;\r\n}\r\n\r\n#ing-comp.arthrid-ing {\r\n  background: url('/assets/products/arthrid/ing-bg.png');\r\n  background-attachment: fixed;\r\n  background-size: unset;\r\n}\r\n\r\n._blue-header {\r\n\tcolor: #4c5a9c;\r\n}\r\n\r\n._green-header {\r\n\tcolor: #79aa5b;\r\n}\r\n\r\n._darkgreen-header {\r\n\tcolor: #2db573;\r\n}\r\n\r\n.broken-lines {\r\n\tborder: dashed blue 2px;\r\n\tdisplay: flex;\r\n\tjustify-content: center;\r\n\talign-items: center;\r\n\ttext-align: center;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9zdHlsZXMuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLCtFQUErRTs7QUFFL0U7O3FFQUVxRTs7QUFFckU7Q0FDQyx3QkFBd0I7Q0FDeEIsZUFBZTtDQUNmLDZDQUE2QztDQUM3QyxxQ0FBcUM7Q0FDckMsbUJBQW1CO0NBQ25COztBQUVEO0NBQ0Msd0JBQXdCO0NBRXhCLDJCQUEyQjtDQUMzQix5QkFBeUI7Q0FDekIseUJBQXlCO0NBQ3pCOztBQUVEO0NBQ0MsMEJBQTBCO0NBQzFCLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEI7O0FBRUQ7Q0FDQyxzQkFBc0I7Q0FDdEIsU0FBUztDQUNULFlBQVk7Q0FDWiw4QkFBOEI7Q0FDOUI7O0FBRUQ7Ozs7Q0FJQyx1Q0FBdUM7Q0FDdkM7O0FBRUQ7Q0FDQyw0QkFBNEI7Q0FDNUIsZ0RBQWdEO0NBQ2hEOztBQUVEO0NBQ0MsMkJBQTJCO0NBQzNCLHNEQUFzRDtDQUN0RDs7QUFFRDtDQUNDLFdBQVc7Q0FDWDs7QUFFRDtDQUNDLFVBQVU7Q0FDVjs7QUFJRDtDQUNDLDRCQUE0QjtDQUM1QixpQkFBaUI7Q0FDakI7O0FBSUQ7Q0FDQyxvQkFBb0I7Q0FDcEI7O0FBRUQ7Q0FDQyxxQ0FBcUM7Q0FDckM7O0FBRUQ7Q0FDQyxxQ0FBcUM7Q0FDckM7O0FBRUQ7Q0FDQywyQ0FBMkM7Q0FDM0MsdUVBQXVFO0NBQ3ZFLDRCQUE0QjtDQUM1Qix1QkFBdUI7Q0FDdkIsNkJBQTZCO0NBQzdCOztBQUVEO0NBQ0MsYUFBYTtDQUNiLGNBQWM7Q0FDZDs7QUFFRDtDQUNDLG1CQUFtQjtDQUNuQjs7QUFFRDtDQUNDLGVBQWU7Q0FDZjs7QUFFRDtDQUNDLHNEQUFzRDtDQUN0RCw2QkFBNkI7Q0FDN0IsdUJBQXVCO0NBQ3ZCLGlDQUFpQztDQUNqQyw2QkFBNkI7Q0FDN0I7O0FBRUQ7RUFDRSx1REFBdUQ7RUFDdkQsNkJBQTZCO0VBQzdCLHVCQUF1QjtDQUN4Qjs7QUFFRDtDQUNDLGVBQWU7Q0FDZjs7QUFDRDtDQUNDLGVBQWU7Q0FDZjs7QUFFRDtDQUNDLGVBQWU7Q0FDZjs7QUFFRDtDQUNDLHdCQUF3QjtDQUN4QixjQUFjO0NBQ2Qsd0JBQXdCO0NBQ3hCLG9CQUFvQjtDQUNwQixtQkFBbUI7Q0FDbkIiLCJmaWxlIjoic3JjL3N0eWxlcy5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBZb3UgY2FuIGFkZCBnbG9iYWwgc3R5bGVzIHRvIHRoaXMgZmlsZSwgYW5kIGFsc28gaW1wb3J0IG90aGVyIHN0eWxlIGZpbGVzICovXHJcblxyXG4vKiBAaW1wb3J0ICcvYXBwL2NvbXBvbmVudHMvcHJvZHVjdHMvYXJ0aHJpZC9hcnRocmlkLmNvbXBvbmVudC5jc3MnO1xyXG5AaW1wb3J0ICcvYXBwL2NvbXBvbmVudHMvcHJvZHVjdHMvYXJ0aHJpZC9tZW1vcGx1c2dvbGQuY29tcG9uZW50LmNzcyc7XHJcbkBpbXBvcnQgJy9hcHAvY29tcG9uZW50cy9wcm9kdWN0cy9hcnRocmlkL2NvZ2VudGRiLmNvbXBvbmVudC5jc3MnOyAqL1xyXG5cclxuYm9keSB7XHJcblx0LyogbWF4LXdpZHRoOiAxODAwcHg7ICovXHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0LyogZm9udC1mYW1pbHk6ICdBdmFudGUtQmxhY2snICFpbXBvcnRhbnQ7ICovXHJcblx0Zm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmO1xyXG5cdG92ZXJmbG93LXg6IGhpZGRlbjtcclxufVxyXG5cclxuLmxpbmsge1xyXG5cdGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG5cdC13ZWJraXQtdHJhbnNpdGlvbjogZm9udC1zaXplIDJzO1xyXG5cdC8qIEZvciBTYWZhcmkgMy4wIHRvIDYuMCAqL1xyXG5cdHRyYW5zaXRpb246IGZvbnQtc2l6ZSAycztcclxuXHQvKiBGb3IgbW9kZXJuIGJyb3dzZXJzICovXHJcbn1cclxuXHJcbi5saW5rOmhvdmVyIHtcclxuXHR0ZXh0LXNoYWRvdzogMnB4IDJweCBncmF5O1xyXG5cdGZvbnQtc2l6ZTogMTEwJTtcclxuXHRmb250LXdlaWdodDogYm9sZGVyO1xyXG59XHJcblxyXG4uZGl2aWRlciB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHdpZHRoOiAwO1xyXG5cdGhlaWdodDogM2VtO1xyXG5cdGJvcmRlci1yaWdodDogd2hpdGUgc29saWQgM3B4O1xyXG59XHJcblxyXG5oMSxcclxuaDIsXHJcbmgzLFxyXG5oNCB7XHJcblx0Zm9udC1mYW1pbHk6ICdDb29wZXJIZXdpdHQnICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbkBmb250LWZhY2Uge1xyXG5cdGZvbnQtZmFtaWx5OiBcIkNvb3Blckhld2l0dFwiO1xyXG5cdHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Db29wZXJIZXdpdHQtQm9sZC5vdGZcIik7XHJcbn1cclxuXHJcbkBmb250LWZhY2Uge1xyXG5cdGZvbnQtZmFtaWx5OiBcIkF2YW50ZS1Cb2xkXCI7XHJcblx0c3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL0lUQ0F2YW50R2FyZGVTdGQtQm9sZENuLm90ZlwiKTtcclxufVxyXG5cclxuLnctMTUge1xyXG5cdHdpZHRoOiAxNSU7XHJcbn1cclxuXHJcbi53LTUge1xyXG5cdHdpZHRoOiA1JTtcclxufVxyXG5cclxuXHJcblxyXG4udWstY2FyZC10aXRsZSB7XHJcblx0Zm9udC1zaXplOiAxLjJlbSAhaW1wb3J0YW50O1xyXG5cdGxpbmUtaGVpZ2h0OiAxLjQ7XHJcbn1cclxuXHJcblxyXG5cclxuLnJvdW5kLWJvcmRlciB7XHJcblx0Ym9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG5cclxuLmdyZWVuIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZlNWFlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZWFsIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmOWFlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5wb3dkZXJibHVlIHtcclxuXHQvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjNDhjNGQzICFpbXBvcnRhbnQ7ICovXHJcblx0YmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL3Byb2R1Y3RzL2FydGhyaWQvYXJ0aHJpZC1ncmFkaWVudC5wbmcnKTtcclxuXHRiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG59XHJcblxyXG4jbGFuZGluZ1BhZ2Uge1xyXG5cdHdpZHRoOiAxMDB2dztcclxuXHRoZWlnaHQ6IDEwMHZoO1xyXG59XHJcblxyXG4jbWVudSA+IGxpOm50aC1sYXN0LWNoaWxkKDIpIHtcclxuXHRib3JkZXItcmlnaHQ6IG5vbmU7XHJcbn1cclxuXHJcbi50aXRsZS1oZWFkZXIge1xyXG5cdGNvbG9yOiAjYmUxZTJkO1xyXG59XHJcblxyXG4jaW5nLWNvbXAge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9wcm9kdWN0cy9jb2dlbnQvaW5nLWJnLmpwZycpO1xyXG5cdGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRiYWNrZ3JvdW5kLXBvc2l0aW9uLXk6IGNhbGMoMHB4KTtcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG59XHJcblxyXG4jaW5nLWNvbXAuYXJ0aHJpZC1pbmcge1xyXG4gIGJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9wcm9kdWN0cy9hcnRocmlkL2luZy1iZy5wbmcnKTtcclxuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogdW5zZXQ7XHJcbn1cclxuXHJcbi5fYmx1ZS1oZWFkZXIge1xyXG5cdGNvbG9yOiAjNGM1YTljO1xyXG59XHJcbi5fZ3JlZW4taGVhZGVyIHtcclxuXHRjb2xvcjogIzc5YWE1YjtcclxufVxyXG5cclxuLl9kYXJrZ3JlZW4taGVhZGVyIHtcclxuXHRjb2xvcjogIzJkYjU3MztcclxufVxyXG5cclxuLmJyb2tlbi1saW5lcyB7XHJcblx0Ym9yZGVyOiBkYXNoZWQgYmx1ZSAycHg7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4iXX0= */", '', '']]

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/styles.css":
/*!************************!*\
  !*** ./src/styles.css ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../node_modules/postcss-loader/src??embedded!./styles.css */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./src/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 2:
/*!******************************!*\
  !*** multi ./src/styles.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Odale Roxas\Documents\projects\uiframework\src\styles.css */"./src/styles.css");


/***/ })

},[[2,"runtime"]]]);
//# sourceMappingURL=styles.js.map