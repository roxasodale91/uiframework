// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBLII_NWcX5knryupAp5lU1Lv4fy-hY9Wk',
    authDomain: 'timekeeping-d740f.firebaseapp.com',
    databaseURL: 'https://timekeeping-d740f.firebaseio.com',
    projectId: 'timekeeping-d740f',
    storageBucket: 'timekeeping-d740f.appspot.com',
    messagingSenderId: '1019498750966'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
