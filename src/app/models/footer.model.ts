export class Footer {
  facebookURL: string;
  instagramURL: string;
  twitterURL: string;
  lazadaURL: string;
  shopeeURL: string;
  youtubeURL: string;
  product: string;
}
