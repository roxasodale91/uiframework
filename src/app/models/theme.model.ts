export class Theme {
  navbarColor: string;
  footerColor: string;
  sidePanelColor: string;
  navbarFontColor: string;
  footerFontColor: string;
  sidePanelFontColor: string;
  navbarLogoURL: string;
}
