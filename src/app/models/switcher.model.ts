export interface Switcher {
  title: string;
  subTitle: string;
  bodyTitle: string;
  bodyType: string;
  bodyContent: any[] | string;
}
