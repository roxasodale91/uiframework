export interface Card {
  title: string;
  body: any;
  mediaType: string;
  titleClass?: string[];
  bodyClass?: string[];
  cardClass?: string[];
}
