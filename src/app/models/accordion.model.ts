export interface Accordion {
  title: string;
  body: any;
  titleClass?: string[];
  bodyClass?: string[];
  cardClass?: string[];
}
