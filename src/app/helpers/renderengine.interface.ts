export interface IRenderEngine {
  renderContainerElement(obj: any, viewContainerRef);
  renderContainerElements(obj: any[], viewContainerRef);
}
