import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopbtnComponent } from './shopbtn.component';

describe('ShopbtnComponent', () => {
  let component: ShopbtnComponent;
  let fixture: ComponentFixture<ShopbtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopbtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopbtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
