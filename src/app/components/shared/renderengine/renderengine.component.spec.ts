/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RenderengineComponent } from './renderengine.component';

describe('RenderengineComponent', () => {
  let component: RenderengineComponent;
  let fixture: ComponentFixture<RenderengineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenderengineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenderengineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
