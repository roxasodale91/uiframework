/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CogentDiabetes2Component } from './cogent-diabetes2.component';

describe('CogentDiabetes2Component', () => {
  let component: CogentDiabetes2Component;
  let fixture: ComponentFixture<CogentDiabetes2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CogentDiabetes2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CogentDiabetes2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
