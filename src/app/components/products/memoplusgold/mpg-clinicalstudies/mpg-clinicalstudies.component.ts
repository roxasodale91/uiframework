import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mpg-clinicalstudies',
  templateUrl: './mpg-clinicalstudies.component.html',
  styleUrls: ['./mpg-clinicalstudies.component.css']
})
export class MpgClinicalstudiesComponent implements OnInit {

  showContent: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  showStudies() {
    this.showContent= true;
  }

}
