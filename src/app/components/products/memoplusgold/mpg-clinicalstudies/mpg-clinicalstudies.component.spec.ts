/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MpgClinicalstudiesComponent } from './mpg-clinicalstudies.component';

describe('MpgClinicalstudiesComponent', () => {
  let component: MpgClinicalstudiesComponent;
  let fixture: ComponentFixture<MpgClinicalstudiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpgClinicalstudiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpgClinicalstudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
