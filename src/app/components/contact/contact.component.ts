import { Component, OnInit } from '@angular/core';
import { Theme } from './../../models/theme.model';
import { Footer } from './../../models/footer.model';
import { ThemingService } from './../../services/theming.service';
import { FooterService } from './../../services/footer.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  footer: Footer = <Footer>{
    facebookURL: 'arthrid.facebook',
    instagramURL: 'arthrid.ig',
    twitterURL: 'arthrid.twitter',
    lazadaURL: 'arthrid.lazada',
    shopeeURL: 'arthrid.shopee',
    youtubeURL: 'arthrid.youtube',
    product: 'arthrid'
  };

  theme: Theme = <Theme> {
    navbarColor: '',
    navbarFontColor: 'white',
    footerColor: '#8cc63f',
    footerFontColor: 'white',
    sidePanelColor: '',
    sidePanelFontColor: ''
  }

  constructor(
    private fos: FooterService,
    private ths: ThemingService) {
    this.fos.footerData = this.footer;
    this.ths.SetThemeValue = this.theme;

    // console.log(this.theme);
  }

  ngOnInit() {
  }

}
