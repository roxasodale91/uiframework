/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ArthridComponent } from './arthrid.component';

describe('ArthridComponent', () => {
  let component: ArthridComponent;
  let fixture: ComponentFixture<ArthridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArthridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArthridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
