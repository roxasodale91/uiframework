/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CogentdbComponent } from './cogentdb.component';

describe('CogentdbComponent', () => {
  let component: CogentdbComponent;
  let fixture: ComponentFixture<CogentdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CogentdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CogentdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
