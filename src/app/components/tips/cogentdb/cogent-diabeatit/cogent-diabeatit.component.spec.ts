/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CogentDiabeatitComponent } from './cogent-diabeatit.component';

describe('CogentDiabeatitComponent', () => {
  let component: CogentDiabeatitComponent;
  let fixture: ComponentFixture<CogentDiabeatitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CogentDiabeatitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CogentDiabeatitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
