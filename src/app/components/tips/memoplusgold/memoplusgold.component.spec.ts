/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MemoplusgoldComponent } from './memoplusgold.component';

describe('MemoplusgoldComponent', () => {
  let component: MemoplusgoldComponent;
  let fixture: ComponentFixture<MemoplusgoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoplusgoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoplusgoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
