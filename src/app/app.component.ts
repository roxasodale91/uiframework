import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../app/components/shared/shared.component.css']
})
export class AppComponent {
  title = 'UIFramework';
}
